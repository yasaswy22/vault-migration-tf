# vault-migration-tf

Mass Migrate Vault credentials from root to a namespace using terraform.

```
Install vault using the official vault tutorial from here https://gitlab.com/yasaswy22/vault-migration-tf.git
You need to download and add to the path in windows to test the Vault CLI
To set path on windows to any .exe follow this https://www.youtube.com/watch?v=AH3QeE_YhiU

# to create a vault dev server in git bash
# https://learn.hashicorp.com/tutorials/vault/getting-started-dev-server
# need this for the server to start on every session
vault server -dev

# in new git bash
# created each time we start the dev server, run all the commands below again with new creds
# if not you get the 'Error enabling: Post "https://127.0.0.1:8200/v1/sys/mounts/dkp": http: server gave HTTP response to HTTPS client'
# to access ui http://127.0.0.1:8200/ui/vault/ and enter the below token id
export VAULT_ADDR=http://127.0.0.1:8200
echo "teWgb59c6ApR7p8a1Otaxfe6/WMyXTMR8kLeBpIT/z4=" > unseal.key
export VAULT_DEV_ROOT_TOKEN_ID=hvs.if2zoicJCsI4en4IxAx3aSo3


# storing the secrets.
vault kv put secret/hello foo=world
vault kv put secret/hello foo=world excited=yes
vault kv get secret/hello
vault kv metadata get secret/foo
vault kv delete secret/hello
vault kv undelete -versions=2 secret/hello
vault kv get secret/foo

# kv secrets engine
# by default vaule enables engine kv
vault secrets enable -path=dkp-test kv
vault secrets enable kv
vault secrets list
vault kv put dkp-test/hello govid=12724318
vault kv get dkp-test/hello
vault kv put dkp-test/ingame-details govid=12724318
vault kv get dkp-test/ingame-details
vault kv list dkp-test/
vault secrets disable kv/
vault secrets list

# dynamic secrets engine
# kv is putting data, storing ourselves
# dynamic secrets are generated when accessed
# dynamic secrets do not exist until they're read, revoked after use
# Note: if secret_key has a / in it, it was throwing an error
vault secrets enable -path=aws-dkp-test/ aws
vault write aws-dkp-test/config/root \
access_key=AKIAW77HYQCVPD637ONI \
secret_key=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 \
region=us-east-1

# engine will use these credentials to communicate with aws in future requests
# next step is to configure a role, human friendly identifier to an action
# map the IAM policy to a named role
# below tells vaule, when i ask for credential for my-dkp-role, 
# create and attach the IAM policy below
vault write aws-dkp-test/roles/my-dkp-role \
credential_type=iam_user \
policy_document=-<<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1426528957000",
      "Effect": "Allow",
      "Action": [
        "ec2:*"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF

# ask vault to generate access key pair for the role
vault read aws-dkp-test/roles/my-dkp-role
vault read aws-dkp-test/creds/my-dkp-role
vault lease lookup aws-dkp-test/creds/my-dkp-role/CBXqzEbY0VX1AhUC3tMaUmKQ
vault lease revoke aws-dkp-test/creds/my-dkp-role/CBXqzEbY0VX1AhUC3tMaUmKQ

# for help documentation
vault path-help aws-dkp-test
vault path-help aws-dkp-test/creds/my-dkp-role

```

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/yasaswy22/vault-migration-tf.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/yasaswy22/vault-migration-tf/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
